<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<c:if test="${not empty sessionScope.customer} ">
	<c:redirect url="catalogo.jsp" />
</c:if>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registro de Usuarios</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

<script>
	function check(confirm) {
		if (confirm.value != document.getElementById('password').value) {
			confirm.setCustomValidity('Las contraseñas no coinciden');
		} else
			confirm.setCustomValidity('');
	}
</script>
</head>
<body>
<nav>
    <div>

        <span></span>
      <div>
      <ul>
      <c:choose>
      <c:when test="${not empty sessionScope.customer}">
        
          <li>
            <a href="#">${sessionScope.customer}</a>
          </li>

          <li>
            <a href="logout">Cerrar sesion</a>
          </li>
        
        </c:when>
        <c:otherwise>

		</c:otherwise>		
       </c:choose>
       </ul>
      </div>
    </div>
  	</nav>
  	<div >
  	<div>
  	<div >
	<form action="registro" method="post" onsubmit="check()">
		<h1>Registro de usuario</h1>

		<p>
			<label for="nombre">Nombre</label>
		</p>
		<p>
			<input type="text" name="nombre" required="required" />
		</p>
		<p>
			<label for="apellidos">Apellidos</label>
		</p>
		<p>
			<input type="text" name="apellidos" required="required" />
		</p>
		<p>
			<label for="email">Correo electronico</label>
		</p>
		<p>
			<input type="email" name="email" required="required" />
		</p>
		<p>
			<label for="password">Contraseña</label>
		</p>
		<p>
			<input type="password" name="password" id="password"
				required="required" />
		</p>
		<p>
			<label for="password">Confirmar contraseña</label>
		</p>
		<p>
			<input type="password" id="confirm" required="required"
				oninput="check(this)" />
		</p>
		<p>
			<input type="submit" value="Registrarse" />
		</p>
	</form>

           		<a href="login.jsp">Iniciar Sesion</a>


            	<a href="catalogo.jsp">Volver al catalogo</a>

	<c:choose>
		<c:when test="${param.status == 1}">
			<p>El email ya esta registrado</p>
		</c:when>
		<c:when test="${param.status == 2}">
			<p>Error del sistema, contacte con el administrador</p>
		</c:when>
	</c:choose>
	</div>
	</div>
	</div>
	
</body>
</html>
