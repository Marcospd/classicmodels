<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>
<%
if (session.getAttribute("usuario") != null)
	response.sendRedirect("catalogo");
else {
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inicio de Sesi�n</title>
</head>
<body>
	<form action="auth" method="post">
		<p>
			<label for="email">eMail</label>
		</p>
		<p>
			<input type="email" name="email" required="required" />
		</p>
		<p>
			<label for="password">Contrase�a</label>
		</p>
		<p>
			<input type="password" name="password" required="required" />
		</p>
		<p>
			<input type="submit" value="Login" />
		</p>
		<a class="" href="catalogo.jsp">Volver</a>
	</form>
</body>
</html>
<%
}
%>
