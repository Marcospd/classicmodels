<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<c:choose>
	<c:when test="${not empty sessionScope.usuario}">
		<c:redirect url="catalogo.jsp" />
	</c:when>
	<c:otherwise>
		<%
		String email = request.getParameter("email");
		if (email == null)
			response.sendRedirect("catalogo.jsp");
		else {
		%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Registro</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

</head>
<body>

	<nav>
    <div>
        <span></span>
      <div>
      <ul>
      <c:choose>
      <c:when test="${not empty sessionScope.customer}">
        
          <li>
            <a href="#">${sessionScope.customer}</a>
          </li>

        
        </c:when>
        <c:otherwise>

		</c:otherwise>		
       </c:choose>
       </ul>
      </div>
    </div>
  	</nav>

	<div >
  		<div >
  			<div>
				<h1>
					Bienvenido
				</h1>
				<p>
					Sólo queda un paso más para completar tu registro y tener acceso a
					todos nuestros Servicios, sigue las instrucciones que te hemos enviado
					a
					<%=email%></p>
				<p>Pulsa el botón de reenviar para que te volvamos a enviar el
					mensaje de confirmación:</p>
				<form action="reenvio" method="post">
					<input type="hidden" name="email" value="<%=email%>" />
					<p>
						<input class="btn btn-primary" type="submit" value="Reenviar" />
					</p>
				</form>
				        	<li>
           		<a href="login.jsp">Iniciar Sesion</a>
          	</li>
          	<li>
            	<a  href="registro.jsp">Registrarse</a>
          	</li>
			</div>
		</div>
	</div>
</body>
</html>
<%
}
%>
</c:otherwise>
</c:choose>
